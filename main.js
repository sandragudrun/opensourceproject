const container = document.querySelector(".container");
const cardArray = [
  {
    title: "This is a Title",
    imageUrl:
      "http://news.mit.edu/sites/mit.edu.newsoffice/files/images/2016/MIT-Earth-Dish_0.jpg",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo mi, laoreet a pretium et, semper quis dui. Fusce feugiat finibus metus, ut consequat urna scelerisque eget. Vestibulum a turpis nec eros vestibulum hendrerit eu iaculis magna. Morbi sit amet congue neque, vitae tincidunt nulla."
  },
  {
    title: "This is a another Title",
    imageUrl: "https://www.w3schools.com/w3css/img_lights.jpg",
    content:
      "Sed interdum pulvinar nibh sed iaculis. Phasellus venenatis lorem sed varius accumsan. Morbi at velit suscipit, tincidunt nibh placerat."
  },
  {
    title: "This is the thrid Title",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6CgVGg5OjY7d64jvpXLs4MsWpykLmzOa_I6ClH2zLvycZ5J2c4g",
    content:
      "Phasellus consequat felis sit amet facilisis consectetur. Mauris mollis eget nulla in tempor."
  },
  {
    title: "This is the thrid Title",
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6CgVGg5OjY7d64jvpXLs4MsWpykLmzOa_I6ClH2zLvycZ5J2c4g",
    content:
      "Phasellus consequat felis sit amet facilisis consectetur. Mauris mollis eget nulla in tempor."
  },
  {
    title: "This is the fourth fourth of titles",
    imageUrl:
      "http://www.visithunathing.is/static/news/1521627511_800px-hvammstangakirkja.jpg",
    content:
      "Phasellus Phasellus Phasellus Phasellus Phasellus."
  },
  {
    title: "All hail the Smiley Cat",
    imageUrl:
      "https://www.dailydot.com/wp-content/uploads/2018/10/olli-the-polite-cat.jpg",
    content:
      "Fí fæ fó fam!"
  }

];
// function sem keyrir öll önnur functions og methods til þess að initialyza forritið
const init = function() {
  cardBuilder();
};
function cardBuilder() {
  let str = "";

  cardArray.forEach(card => {
    str = returnCards(card);
    container.innerHTML += str;
  });
}
const returnCards = function(card) {
  return `
      <div class="card">
          <h1 class="card__title">${card.title}</h1>
          <img src="${card.imageUrl}" alt="" class="card__img"/>
          <p class"card__content">${card.content}</p>
      </div>`;
};
init();
module.exports = {
  cardArray
};
